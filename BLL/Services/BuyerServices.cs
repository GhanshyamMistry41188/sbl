﻿using System;
using System.Collections.Generic;
using System.Text;
using DAL.Models;
using BLL.Models;
using System.Linq;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using System.Data;

namespace BLL.Services
{
    public class BuyerServices
    {
        private readonly dbInterviewContext dbInterviewContext = new dbInterviewContext();
       
        public void BuyEvent(BuyerModel buyerModel, out bool Status, out string Message)
        {            
            SqlParameter BuyerName = new SqlParameter("@BuyerName", buyerModel.BuyerName);
            SqlParameter EventId = new SqlParameter("@EventId", buyerModel.EventId);
            SqlParameter TesterKey = new SqlParameter("@TesterKey", "gmistry41188");

            var _sMsg = new SqlParameter("Message", "")
            {
                Direction = ParameterDirection.Output,
                DbType = DbType.String,
                Size = 500
            };

            var _sStatus = new SqlParameter("Status", false)
            {
                Direction = ParameterDirection.Output,
                DbType = DbType.Boolean
            };

            var sql = "uspBuyEvent_gmistry41188 @BuyerName, @EventId, @TesterKey, @Status OUTPUT, @Message OUTPUT";
            var dr = dbInterviewContext.Database.ExecuteSqlRaw(sql, BuyerName, EventId, TesterKey, _sStatus, _sMsg);

            Message = Convert.ToString(_sMsg.Value);
            Status = Convert.ToBoolean(_sStatus.Value);
        }

        public int GetCountdownTime()
        {
            var data = dbInterviewContext.Event.FirstOrDefault(h => h.EventId == 1);
            if (data != null)
            {
                return data.TimeoutInSeconds;
            }
            else
            {
                return 0;
            }
        }
    }
}
