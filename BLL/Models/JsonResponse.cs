﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BLL.Models
{
    public class JsonResponse
    {
        public bool Status { get; set; }
        public string Message { get; set; }
        public Object Object { get; set; }
    }
}
