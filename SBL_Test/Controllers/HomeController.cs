﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using SBL_Test.Models;
using BLL.Models;
using BLL.Services;

namespace SBL_Test.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly BuyerServices buyerServices;

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
            buyerServices = new BuyerServices();
        }

        public IActionResult Index()
        {
            var Buyer = new BuyerModel();
            ViewBag.TimeoutInSeconds = buyerServices.GetCountdownTime();
            return View(Buyer);
        }

        [HttpPost]
        public JsonResult BuyEvent(BuyerModel buyerModel)
        {
            JsonResponse jsonResponse = new JsonResponse();
            try
            {
                string Message = string.Empty;
                bool Status = false;
                buyerServices.BuyEvent(buyerModel, out Status, out Message);

                jsonResponse.Status = Status;
                jsonResponse.Message = Message;
            }
            catch (Exception ex)
            {
                jsonResponse.Status = false;
                jsonResponse.Message = ex.Message;
            }
            return Json(jsonResponse);
        }
    }
}
